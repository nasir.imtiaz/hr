package com.esrinea.hr.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.esrinea.hr.domain.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
	List<Employee> findByIsDeleted(Boolean isDeleted);
	Employee findByUsername(String userName);
}