package com.esrinea.hr.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.esrinea.hr.domain.Department;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Integer> {
	List<Department> findAllByOrderByNameAsc();
}
