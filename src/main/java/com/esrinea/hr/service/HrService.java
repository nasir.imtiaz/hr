package com.esrinea.hr.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.esrinea.hr.domain.Department;
import com.esrinea.hr.domain.Employee;
import com.esrinea.hr.dto.DepartmentDto;
import com.esrinea.hr.dto.EmployeeDto;
import com.esrinea.hr.repository.DepartmentRepository;
import com.esrinea.hr.repository.EmployeeRepository;

@Service
public class HrService {
	
	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	private DepartmentRepository departmentRepository;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	public List<EmployeeDto> getEmployees(boolean showDeletedRecords) {
		List<EmployeeDto> employeesDto = new ArrayList<>();
		List<Employee> employees = employeeRepository.findByIsDeleted(showDeletedRecords);
		
		if (employees != null && employees.size() > 0) {
			employeesDto = employees.stream().map(EmployeeDto::new).collect(Collectors.toList());
		}
		
		return employeesDto;
	}
	
	public List<DepartmentDto> getDepartments() {
		List<DepartmentDto> departmentsDto = null;
		List<Department> departments = departmentRepository.findAllByOrderByNameAsc();
		
		if (departments != null && departments.size() > 0) {
			departmentsDto = departments.stream().map(DepartmentDto::new).collect(Collectors.toList());
		}
		
		return departmentsDto;
	}
	
	public String saveEmployee(EmployeeDto employeeDto) {
		Employee employee;

		if (employeeDto.getUsername().equals("admin")) {
			throw new RuntimeException("Username 'admin' is reserved for the system.");			
		}
		
		if (employeeDto.getId() == null) {
			employee = new Employee();
			employee.setPassword(passwordEncoder.encode(employeeDto.getPassword()));
		} else {
			employee = employeeRepository.getOne(employeeDto.getId());
			
			if (employeeDto.getPassword() != null && employeeDto.getPassword().trim().length() > 0)
				employee.setPassword(passwordEncoder.encode(employeeDto.getPassword()));
		}
		
		employee.setFirstName(employeeDto.getFirstName());
		employee.setLastName(employeeDto.getLastName());
		employee.setUsername(employeeDto.getUsername());
		employee.setSkills(employeeDto.getSkills());

		Department department = new Department();
		department.setId(employeeDto.getDepartmentId());
		
		employee.setDepartment(department);
		
		try {
			employeeRepository.save(employee);
		} catch (DataIntegrityViolationException e) {
			throw new RuntimeException(String.format("A record with Username: '%s' already exists. Please choose a different Username.", employeeDto.getUsername()));
		}
		
		if (employeeDto.getId() == null)
			return "Employee record saved successfully.";
		else
			return "Employee record updated successfully.";
	}
	
	public EmployeeDto getEmployee(Integer employeeId) {
		Optional<Employee> employee = employeeRepository.findById(employeeId);
		
		if (employee.isPresent()) {
			EmployeeDto employeeDto = new EmployeeDto(employee.get());
			employeeDto.setPassword("");
			return employeeDto;
		}
		
		throw new RuntimeException("No records found.");
	}
	
	public String toggleDeleteEmployee(Integer employeeId) {
		Employee employee = employeeRepository.getOne(employeeId);
		employee.setIsDeleted( !employee.getIsDeleted() );
		employeeRepository.save(employee);
		
		if (employee.getIsDeleted())
			return "Employee record deleted successfully.";
		else
			return "Employee record restored successfully.";
	}
	
	public EmployeeDto getEmployeeByUsername(String userName) {
		Employee employee = employeeRepository.findByUsername(userName);

		if (employee != null)
			return new EmployeeDto(employee);
		
		return null;
	}
}
