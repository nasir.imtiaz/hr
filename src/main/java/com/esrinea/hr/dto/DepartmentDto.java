package com.esrinea.hr.dto;

import com.esrinea.hr.domain.Department;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DepartmentDto {
	private Integer id;
	private String name;
	
	public DepartmentDto(Department department) {
		this.id = department.getId();
		this.name = department.getName();
	}
}
