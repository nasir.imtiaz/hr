package com.esrinea.hr.dto;

import com.esrinea.hr.domain.Employee;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class EmployeeDto {
	private Integer id;
	private String firstName;
	private String lastName;
	private String skills;
	private String username;
	private String password;
	private String departmentName;
	private Integer departmentId;
	private boolean isDeleted;
	
	public EmployeeDto(Employee employee) {
		this.id = employee.getId();
		this.firstName = employee.getFirstName();
		this.lastName = employee.getLastName();
		this.skills = employee.getSkills();
		this.username = employee.getUsername();
		this.password = employee.getPassword();
		this.departmentId = employee.getDepartment().getId();
		this.departmentName = employee.getDepartment().getName();
		this.isDeleted = employee.getIsDeleted();
	}
}
