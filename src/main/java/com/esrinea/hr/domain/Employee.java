package com.esrinea.hr.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "emp_id")
	private Integer id;

	private String firstName;
	private String lastName;
	private String skills;
	
	@Column(unique=true)
	private String username;
	
	private String password;

	private Boolean isDeleted = false;
	
	@ManyToOne
	@JoinColumn(name = "dept_id")
	private Department department;
}
