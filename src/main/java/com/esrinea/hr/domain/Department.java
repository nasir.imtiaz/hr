package com.esrinea.hr.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Department {
	@Id
	@Column(name = "dept_id")
	private Integer id;

	private String name;
	
	@OneToMany(mappedBy = "department")
	private List<Employee> employees = new ArrayList<>();
}
