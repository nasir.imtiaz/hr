package com.esrinea.hr.config.security;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.esrinea.hr.dto.EmployeeDto;
import com.esrinea.hr.service.HrService;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
	
	@Autowired
	private HrService hrService;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
    @Override
    public Authentication authenticate(Authentication authentication) 
      throws AuthenticationException {
 
        String userName = authentication.getName();
        String password = authentication.getCredentials().toString();

        if ("admin".equals(userName) && "admin".equals(password)) {
        	CustomUserDetails principal = new CustomUserDetails(userName, password, getGrantedAuthorities("ROLE_ADMIN"));
        	return new UsernamePasswordAuthenticationToken(principal, password, principal.getAuthorities());
        } else {
        	EmployeeDto employee = hrService.getEmployeeByUsername(userName);
        	
        	if (employee != null && !employee.isDeleted()) {
        		if (passwordEncoder.matches(password, employee.getPassword())) {
                	CustomUserDetails principal = new CustomUserDetails(userName, password, getGrantedAuthorities("ROLE_EMPLOYEE"));
                	principal.setEmployeeId(employee.getId());
        			return new UsernamePasswordAuthenticationToken(principal, authentication.getCredentials(), principal.getAuthorities());
        		}
        	}
        }
        
        throw new BadCredentialsException("");        		
    }
 
    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
    
    private List<GrantedAuthority> getGrantedAuthorities(String authority) {
    	return Arrays.asList(new SimpleGrantedAuthority(authority));
    }
}