package com.esrinea.hr.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.esrinea.hr.dto.DepartmentDto;
import com.esrinea.hr.dto.EmployeeDto;
import com.esrinea.hr.service.HrService;

@RestController
public class HrController {
	
	private Logger logger = LoggerFactory.getLogger(HrController.class);
	
	@Autowired
	private HrService hrService;

	@GetMapping("/employees")
	@Secured("ROLE_ADMIN")
	public List<EmployeeDto> employees(@RequestParam(required = false) boolean showDeletedEmployees) {
		logger.info("Retrieving Employees list with showDeletedEmployees flag: {}", showDeletedEmployees);
		return hrService.getEmployees(showDeletedEmployees);
	}

	@GetMapping("/departments")
	@Secured("ROLE_ADMIN")
	public List<DepartmentDto> departments() {
		logger.info("Retrieving Departments");
		return hrService.getDepartments();
	}
	
	@PostMapping("/employee")
	@Secured("ROLE_ADMIN")
	public String saveEmployee(@RequestBody EmployeeDto employee) {
		logger.info("Saving Employee: {}", employee);
		return hrService.saveEmployee(employee);
	}
	
	@PutMapping("/employee/{employeeId}")
	@Secured("ROLE_ADMIN")
	public String updateEmployee(@RequestBody EmployeeDto employee, @PathVariable Integer employeeId) {
		logger.info("Updating Employee: {}", employee);
		employee.setId(employeeId);
		return hrService.saveEmployee(employee);
	}
	
	@DeleteMapping("/employee/{employeeId}")
	@Secured("ROLE_ADMIN")
	public String toggleDeleteEmployee(@PathVariable Integer employeeId) {
		logger.info("Deleting Employee with Id: {}", employeeId);
		return hrService.toggleDeleteEmployee(employeeId);
	}
	
	@GetMapping("/employee/{employeeId}")
	@Secured({"ROLE_ADMIN", "ROLE_EMPLOYEE"})
	public EmployeeDto employee(@PathVariable Integer employeeId) {
		logger.info("Retrieving Employee data against Id: {}", employeeId);
		return hrService.getEmployee(employeeId);
	}
}