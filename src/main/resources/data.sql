DROP TABLE IF EXISTS employee;
DROP TABLE IF EXISTS department;

CREATE TABLE department (
	dept_id INT AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(250) NOT NULL
);

CREATE TABLE employee (
  emp_id INT AUTO_INCREMENT PRIMARY KEY,
  first_name VARCHAR(250) NOT NULL,
  last_name VARCHAR(250) NOT NULL,
  skills VARCHAR(250) NOT NULL,
  username VARCHAR(250) NOT NULL UNIQUE,
  password VARCHAR(250) NOT NULL,
  is_deleted BOOLEAN NOT NULL,
  dept_id INT  NOT NULL
);

ALTER TABLE employee ADD FOREIGN KEY (dept_id) REFERENCES department(dept_id);

INSERT INTO department (name) values ('Operations');
INSERT INTO department (name) values ('Marketing');
INSERT INTO department (name) values ('Accounting and Finance');
INSERT INTO department (name) values ('Human Resource Management');