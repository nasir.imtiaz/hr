function onLoad(employeeId) {
	$.ajax({
        url: `/employee/${employeeId}`,
        type: "GET",
        success: function (response) {
        	$("#department").val(response.departmentName);
        	$("#firstName").val(response.firstName);
        	$("#lastName").val(response.lastName);
        	$("#username").val(response.username);
        	response.skills.split(",").forEach(value => addSkill(value.trim()));
        },
        error: function (error) {
            toast('error', error.responseJSON.message);
        }
    });
}

function addSkill(skill) {
	$('#skillTags').append(`<span class="badge badge-secondary mr-1"><span>${skill}</span>`);
}