$(document).ready(function() {
	initializeEmployeeGrid();
	initializeDepartments();

	$("#toggleHandle").html('Deleted Records <input type="checkbox" id="toggleDeletedEmployees">');
	
	$('#toggleDeletedEmployees').bootstrapToggle({
		on: 'Show',
		off: 'Hide'
	});
	
	$('#toggleDeletedEmployees').change(function() {
		initializeEmployeeGrid( $(this).prop('checked') );
	});
	
	intializeToolTip();
});

function intializeToolTip() {
	$('body').tooltip({
	    selector: '.tooltip-link'
	});
}

function initializeEmployeeGrid(showDeletedEmployees=false) {
	const url = `/employees?showDeletedEmployees=${showDeletedEmployees}`;
	
	if ($.fn.DataTable.isDataTable('#employeeGrid')) {
		const employeeGrid = $('#employeeGrid').DataTable();
		employeeGrid.ajax.url(url).load();
		return;
	}

	$('#employeeGrid')
		.DataTable({
			language: {
		        emptyTable: "No employee records to show"
		    },
			ajax: {
				url: url,
				dataSrc: ""
			},
			columns: [
				{ data : "firstName" },
				{ data : "lastName" },
				{
					data : function(row, type, val, data) {
						return row.skills.split(",").map(skill => `<span class="badge badge-secondary mr-1">${skill}</span>`).reduce((accumulator, currentValue) => `${accumulator}${currentValue}`);
					}
				},
				{ data : "departmentName" },
				{ data : "username" },
				{
					orderable: false,
					data : function(row, type, val, data) {
						const editHtml = `<a href="#" onclick="onEdit(${row.id})" class="btn btn-sm btn-outline-secondary"><i class="fa fa-pencil mr-1"></i>Edit</a>`;
						const deleteHtml = `<a href="#" onclick="toggleDeleteEmployee(${row.id})" class="ml-1 btn btn-sm  btn-outline-danger"><i class="fa fa-trash mr-1"></i>Delete</a>`;
						const rollbackHtml = `<a href="#" onclick="toggleDeleteEmployee(${row.id}, true)" class="btn btn-sm btn-outline-success"><i class="fa fa-undo mr-1"></i>Restore</a>`;
						const actionHtml = row.deleted ? `${rollbackHtml}` : `${editHtml}${deleteHtml}`;
						
						return actionHtml;
					}
				}
			],
			dom: "<'d-flex justify-content-end mb-2'<'#toggleHandle'>>" + "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>"
		});
}

function initializeDepartments() {
	$.ajax({
        url: '/departments',
        type: "GET",
        success: function (response) {
        	response.forEach(function(department) {
				$('#department').append(`<option value='${department.id}'>${department.name}</option>`);
			});
        },
        error: function (error) {
            console.error(error);
        }
    });
}

$('#skills').keypress(function(event) {
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if (keycode == '13') {
		addSkill(this.value);
	}
});

function addSkill(skill) {

	if (getSkills().includes(skill)) {
        toast('error', `Skill ${emphasize(skill)} aready added.`);
	} else {
		const skillCount = $("#skillTags").children().length + 1;
		$('#skillTags').append(`<span class="badge badge-secondary mr-1"><span id="skillText${skillCount}">${skill}</span><a href="#" onclick="removeSkill(${skillCount})" class="text-white no-underline"><i class="fa fa-times ml-1"></i></a></span>`);
	}

	$('#skills').val('');
}

function removeSkill(skillCount) {
	$(`#skillText${skillCount}`).parent().remove();
}

function getSkills() {
	const skills = [];
	const skillElements = $("[id^=skillText]");
	
	skillElements.each(function(index) {
		skills.push($(this).text());
	});
	
	return skills;
}

$('#addEditEmployeeBtn').click(function() {
	const skills = getSkills();
	const departmentId = $("#department").val();
	const firstName = $("#firstName").val();
	const lastName = $("#lastName").val();
	const username = $("#username").val();
	const password = $("#password").val();
	const retypePassword = $("#retypePassword").val();

	const errorMessages = [];
	
	if (!firstName) {
		errorMessages.push(validationMessages('First Name'));
	}

	if (!lastName) {
		errorMessages.push(validationMessages('Last Name'));
	}

	if (!username) {
		errorMessages.push(validationMessages('Username'));
	}

	if ( $('#addEditEmployeeBtn').val() !== 'Update' ) {
		
		if (!password) {
			errorMessages.push(validationMessages('Password'));
		}
		
		if (!retypePassword) {
			errorMessages.push(validationMessages('Retype Password'));
		}
		
	}

	if (password && retypePassword && password!=retypePassword) {
		errorMessages.push(`${emphasize('Password')} and ${emphasize('Retype Password')} fields must match.`);
	}
	
	if (!departmentId) {
		errorMessages.push(validationMessages('Department'));
	}

	if (skills.length == 0) {
		errorMessages.push(`Atleast one ${emphasize('Skill')} is required.`);
	}

	if (errorMessages.length > 0) {
		toast('error', errorMessages.map(message => `<li>${message}</li>`).reduce((accumulator, currentValue) => `${accumulator}${currentValue}`));
	} else {
		
		const employee = {
				firstName: firstName,
				lastName: lastName,
				skills: skills.reduce((accumulator, currentValue) => `${accumulator}, ${currentValue}`),
				username: username,
				password: password,
				departmentId: departmentId					
			};

		let url = '/employee';
		let method = 'POST';
		
		if ( $('#addEditEmployeeBtn').val() === 'Update' ) {
			employee.id = $("#employeeId").val();
			method = "PUT";
			url += `/${employee.id}`;
		} 

		$.ajax({
	        url: url,
	        type: method,
	        data: JSON.stringify(employee),
	        contentType: 'application/json',
	        success: function (response) {
	            $("#addEditEmployeeModal").modal('hide');
				toast('success', response);
				initializeEmployeeGrid();
	        },
	        error: function (error) {
	            toast('error', error.responseJSON.message);
	        }
	    });
	}
});

function toast(type, message, title) {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "0",
        "hideDuration": "0",
        "timeOut": "4000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    
    toastr[type](message, title);
}

function resetForm() {
	$("#department").val('');
	$("#firstName").val('');
	$("#lastName").val('');
	$("#username").val('');
	$("#password").val('');
	$("#retypePassword").val('');
	$('#skillTags').empty();
	$("#addEditEmployeeTitle").text('Add Employee');
	$("#addEditEmployeeBtn").val('Save');
}

function onEdit(employeeId) {
	$.ajax({
        url: `/employee/${employeeId}`,
        type: "GET",
        success: function (response) {
        	$("#employeeId").val(response.id);
        	$("#department").val(response.departmentId);
        	$("#firstName").val(response.firstName);
        	$("#lastName").val(response.lastName);
        	$("#username").val(response.username);
        	$("#password").val(response.password);
        	$("#retypePassword").val(response.password);
        	$('#skillTags').empty();
        	response.skills.split(",").forEach(value => addSkill(value.trim()));
        },
        error: function (error) {
            toast('error', error.responseJSON.message);
        }
    });

	$("#addEditEmployeeTitle").text('Edit Employee');
	$("#addEditEmployeeBtn").val('Update');
	
	$('#addEditEmployeeModal').modal({
        backdrop: 'static',
        keyboard: false
    });	
}

function toggleDeleteEmployee(employeeId, restoreEmployee = false) {
	$.ajax({
        url: `/employee/${employeeId}`,
        type: "DELETE",
        success: function (response) {
			toast('success', response);
			initializeEmployeeGrid(restoreEmployee);
			intializeToolTip();
        },
        error: function (error) {
            toast('error', error.responseJSON.message);
        }
    });
}

function emphasize(text) {
	return `'<b>${text}</b>'`;
}

function validationMessages(text) {
	return `${emphasize(text)} field is required`;
}