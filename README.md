# Human Resource Management

A basic HR system developed using **Spring Boot** integrated with **Spring Security**, **Spring MVC**, **Thymeleaf** and **Spring Data** capabilities. 

The interface is written using **HTML5**, **Bootstrap**, **Javascript** and **jQuery**. 

The project uses in-memory database and in-memory tomcat server.

### Features
- A role based system with Admin and Employee users.
- Redirection to respective landing page based on user role. 
- Responsive Admin landing page interface to create, update, delete, restore and list employees.
- Responsive Empoyee landing page interface to simply view the employee details.
### How to run:
- clone this project into your local directory
```bash
git clone https://gitlab.com/nasir.imtiaz/hr.git
```
- change directory to `hr` and build the project
```bash
hr> mvn clean install package 
```
- switch to `target` directory and run the executable jar file
```bash
hr/target> java -jar hr-0.0.1-SNAPSHOT.jar 
```

This will run the application using the in-memory tomcat server

_Note: The application uses an in-memory database. Therefore, restarting the application will result in loosing the current data._
### How to use:
- The application is accessible through the URL `http://localhost:8080`
- Acessing the above URL will present a Login screen.
- If the Login screen is accessed for the first time after starting the application, then user needs to login as an administrator as there are no employees defined initially. The system has a predefined user with adminintrator role.
- Use the username `admin` and password `admin` to login to the system as an administrator.
- Administrator can:
    - Add a new employee by pressing the 'Add Employee' button
    - View the list of employees
    - Edit an employee
    - Delete an employee
    - Restore an employee by pressing the toggle button
- Once an employee is defined, user can login as employee with the defined credentials.
- Employee can:
    - View its own information
### Webservices:
  - `/employees` - `GET` - retrieves all employees which are not deleted
  - `/employees?showDeletedEmployees=true` - `GET` - retrieves all employees from the datastore which are deleted
  - `/departments` - `GET` - retrieves all departments in the system (these are prepopulated in the database)
  - `/employee/{employeeId}` - `GET` - retrieve an employee by employee id
  - `/employee` - `POST` - creates an employee with the provided JSON of the following form
```
{
   "departmentId":"2",
   "firstName":"Test User First",
   "lastName":"Test User Last",
   "password":"alphabeta",
   "skills":"Test Skill, New Skill",
   "username":"test-user"
}
```
  - `/employee/{employeeId}` - `PUT` - updates an employee against the provided employeeId and JSON of the following form
  ```
{
   "departmentId":"2",
   "firstName":"Test User First",
   "lastName":"Test User Last",
   "password":"alphabeta",
   "skills":"Test Skill, New Skill",
   "username":"test-user"
}
```
  - `/employee/{employeeId}` - `DELETE` - deletes an employee against the provided employeeId. (The API acts as a toggle, where calling the API twice for the same employeeId restores the employee).
### API access by Roles:
- All APIs are accessible to user with ADMIN role.
- User with Employee role can only access `/employee/{employeeId}` - `GET`